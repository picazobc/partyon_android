package com.partyOn;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;


public class MainActivity extends AppCompatActivity {


    public static Context appContext;
    private EditText userEmail, userPass;
    public String url = "";

    private ProgressDialog progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appContext = getApplicationContext();


        userEmail = findViewById(R.id.et_email);
        userPass = findViewById(R.id.et_pass);

        progress = new ProgressDialog(this);
        progress.setMessage("Cargando...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);




        Button btn_login = (Button) findViewById(R.id.btn_signin);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();
                if(!userEmail.getText().toString().isEmpty() && validEmail(userEmail.getText().toString())){
                    if(!userPass.getText().toString().isEmpty()){
                        progress.dismiss();
                        Intent mainIntent = new Intent().setClass( MainActivity.this, SessionActivity.class);
                        startActivity(mainIntent);

                        //intoApplicattion(userEmail.getText().toString(), userPass.getText().toString());


                    }else{

                        Toast.makeText(getApplicationContext(), "Ingresa tu contraseña para continuar",Toast.LENGTH_SHORT).show();

                    }
                }else{

                    userEmail.setError("Campo vacío.");

                }

            }
        });




    }




    public void intoApplicattion(String user, String pass) {

        url = "/login";


         //call callback method
        final AsyncHttpClient client = new AsyncHttpClient();

        Log.d("Login", url );
        client.get(this, url , new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray


                try {
                    Log.d("ENTER", response.toString());

                    response.getJSONObject("estatus");


                    Intent mainIntent = new Intent().setClass( MainActivity.this, SessionActivity.class);
                    startActivity(mainIntent);



                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });

    }



    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }






}
